# FHIR
### Fast Healthcare Interoperability Resources

[HL7® FHIR®](http://hl7.org/fhir) is a next-generation interoperability standard created by the standards development organization Health Level 7 (HL7®). FHIR is designed to enable health data, including clinical and administrative data, to be quickly and efficiently exchanged. 

## Definitions dir
<a href="http://hl7.org/fhir/downloads.html">Downloaded from this page</a>

Various definitions for the FHIR standard, in JSON
XML available at link above
